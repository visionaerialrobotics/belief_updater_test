## belief_updater_test
In order to execute this test, perform the following steps:

- Change directory to this project:

        $ cd $AEROSTACK_STACK/projects_test/belief_updater_test

- Execute the scripts that launch the Aerostack components for this test:

        $ ./main_launcher.sh

- Execute the script that tests the functionality of belief updater and belief manager:

        $ ./belief_updater_test.sh

- To stop the processes execute the following script:

        $ ./stop.sh

- To close the inactive terminals:

        $ killall bash

