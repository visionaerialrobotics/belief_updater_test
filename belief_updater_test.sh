##########################################################################
 #  \brief      Belief manager test
 #  \authors    Javier Cabrera Marugán
 #  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid. All rights reserved
 #
 # Redistribution and use in source and binary forms, with or without
 # modification, are permitted provided that the following conditions are met:
 #
 # 1. Redistributions of source code must retain the above copyright notice,
 #    this list of conditions and the following disclaimer.
 # 2. Redistributions in binary form must reproduce the above copyright notice,
 #    this list of conditions and the following disclaimer in the documentation
 #    and/or other materials provided with the distribution.
 # 3. Neither the name of the copyright holder nor the names of its contributors
 #    may be used to endorse or promote products derived from this software
 #    without specific prior written permission.
 #
 # THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 # "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 # THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 # PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 # CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 # EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 # PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 # OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 # WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 # OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 # EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
##########################################################################
#!/bin/bash
echo "REGULAR TESTS"
# These tests verify that the topics related to the battery and the position work correctly.
# It is needed to wait a second between rostopic and rosservice to have time to add a belief in the memory of beliefs.
# The rosservice command  verifies that the belief has been correctly added.
# When rosservice command is finished the rostopic process is killed with a SIGINT signal.
rostopic pub  /drone111/sensor_measurement/battery_state sensor_msgs/BatteryState '{percentage: 0.2}' > /dev/null &
sleep 1
kill -2 %1
res=$(rosservice call /drone111/query_belief '{query: "battery_level(1,LOW)"}')
if [[ $res == *True* ]]
then
	echo "Test battery_level=LOW: succeeded"
else 
	echo "Test battery_level=LOW: failed"
fi
res=""

rostopic pub  /drone111/sensor_measurement/battery_state sensor_msgs/BatteryState '{percentage: 0.5}' > /dev/null &
sleep 1
kill -2 %1
res=$(rosservice call /drone111/query_belief '{query: "battery_level(1,MEDIUM)"}')
if [[ $res == *True* ]]
then
	echo "Test battery_level=MEDIUM: succeeded"
else 
	echo "Test battery_level=MEDIUM: failed"	
fi
res=""
rostopic pub  /drone111/self_localization/pose geometry_msgs/PoseStamped '{pose: {position: {x: 1.0, y: 0.0, z: 3.0}}}' > /dev/null &
sleep 1
kill -2 %1
res=$(rosservice call /drone111/query_belief '{query: "position(1,(1, 0, 3))"}')
if [[ $res == *True* ]]
then
	echo "Test position=[1.0, 0.0, 3.0]: succeeded"
else 
	echo "Test position=[1.0, 0.0, 3.0]: failed"	
fi
res=""
res=$(rosservice call /drone111/query_belief '{query: "flight_state(1,FLYING)"}')
if [[ $res == *True* ]]
then
	echo "Test flight_state=FLYING: succeeded"
else
	echo "Test flight_state=FLYING: failed"
fi
res=""
rostopic pub   /drone111/sensor_measurement/battery_state sensor_msgs/BatteryState '{percentage: 0.8}' > /dev/null &
sleep 1
kill -2 %1
res=$(rosservice call /drone111/query_belief '{query: "battery_level(1,HIGH)"}')
if [[ $res == *True* ]]
then
	echo "Test battery_level=HIGH: succeeded"
else
	echo "Test battery_level=HIGH: failed"	
fi
res=""
rostopic pub  /drone111/sensor_measurement/battery_state sensor_msgs/BatteryState '{percentage: 0.1}' > /dev/null &
sleep 1
kill -2 %1
res=$(rosservice call /drone111/query_belief '{query: "battery_level(1,LOW)"}')
if [[ $res == *True* ]]
then
	echo "Test battery_level=LOW: succeeded"
else 
	echo "Test battery_level=LOW: failed"	
fi
res=""
rostopic pub  /drone111/sensor_measurement/battery_state sensor_msgs/BatteryState '{percentage: 0.5}' > /dev/null &
sleep 1
kill -2 %1
res=$(rosservice call /drone111/query_belief '{query: "battery_level(1,MEDIUM)"}')
if [[ $res == *True* ]]
then
	echo "Test battery_level=MEDIUM: succeeded"
else 
	echo "Test battery_level=MEDIUM: failed"	
fi
res=""
rostopic pub  /drone111/self_localization/pose geometry_msgs/PoseStamped '{pose: {position: {x: 0.0, y: 0.0, z: 0.0}}}' > /dev/null &
sleep 1
kill -2 %1
res=$(rosservice call /drone111/query_belief '{query: "position(1,(0, 0, 0))"}')
if [[ $res == *True* ]]
then
	echo "Test position=[0.0, 0.0, 0.0]: succeeded"
else 
	echo "Test position=[0.0, 0.0, 0.0]: failed"	
fi
res=""
res=$(rosservice call /drone111/query_belief '{query: "flight_state(1,LANDED)"}')
if [[ $res == *True* ]]
then
	echo "Test flight_state=LANDED: succeeded"
else
	echo "Test flight_state=LANDED: failed"
fi
res=""
rostopic pub  /drone111/self_localization/pose geometry_msgs/PoseStamped '{pose: {position: {x: 8.0, y: 4.0, z: 6.0}}}' > /dev/null &
sleep 1
kill -2 %1
res=$(rosservice call /drone111/query_belief '{query: "position(1,(8, 4, 6))"}')
if [[ $res == *True* ]]
then
	echo "Test position=[8.0, 4.0, 6.0]: succeeded"
else 
	echo "Test position=[8.0, 4.0, 6.0]: failed"	
fi
res=""
res=$(rosservice call /drone111/query_belief '{query: "flight_state(1,FLYING)"}')
if [[ $res == *True* ]]
then
	echo "Test flight_state=FLYING: succeeded"
else
	echo "Test flight_state=FLYING: failed"
fi
res=""
echo ""
echo "EMERGENCY TESTS"
# This test verifies that emergency events work correctly.
# A temporary file is created to save the message and it waits 1 seconds 
# for the emergency event to generate that message.
rostopic echo /drone111/emergency_event > temp.txt & 
rostopic pub -l /drone111/sensor_measurement/battery_state sensor_msgs/BatteryState '{percentage: 0.1}' > /dev/null & 
i=0
while [ ! -s temp.txt ]; 
do
	sleep 1
	i=$((i + 1))
	if [ $i -eq 5 ]; 
	then 
		break
	fi	
done
sleep 1
kill -2 %1
kill -2 %1
emergency=$(cat temp.txt)
rm temp.txt
if [[ $emergency == *LOW* ]]
then
	echo "Test emergency_event found: succeeded"
else 
	echo "Test emergency_event not found: failed"	
fi
